﻿Felipe Velásquez Montoya 201632422

Mario Alejandro Gómez Camargo 201631192

a. ¿Cuál	es	el	orden	de	complejidad	de	buscar	una	parada	específica	en	esta	estructura	de	
datos?	 ¿Cuál	 sería	 el	 orden	 de	 complejidad	 si	 se	 utiliza	 una	 lista	 doblemente	
(sencillamente)	encadenada?
/R: La complejidad de un método que busque una parada específica en nuestro tipo de estructura de datos sería O(N) dado que es dependiente de la cantidad de datos
que contenga la lista. Por otra parte el orden de complejidad en una lista NO circular dobemente (senxillamente) encadenada sería O(N^2) esto es porque sería 
necesario primero ejecutar el método setCurrent().