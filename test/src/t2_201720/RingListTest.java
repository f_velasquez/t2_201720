package t2_201720;

import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.DoubleLinkedList;
import model.data_structures.RingList;
import junit.framework.TestCase;

public class RingListTest extends TestCase{

	private RingList<Integer> listaPrueba;

	private void setUpEscenario1()
	{
		listaPrueba = new RingList<Integer>();
		listaPrueba.add( new Integer (0) );
		listaPrueba.add( new Integer (1) );
		listaPrueba.add(new Integer (2) );
		listaPrueba.add(new Integer (3) );
		listaPrueba.add(new Integer (4) );
		listaPrueba.add( new Integer (6) );
		listaPrueba.add(new Integer (7) );
		listaPrueba.add( new Integer (8) );
		listaPrueba.add( new Integer (9) );
	}

	private void setUpEscenario2()
	{
		listaPrueba = new RingList<Integer>();
		listaPrueba.add( new Integer (8) );
		listaPrueba.add( new Integer (7) );
		listaPrueba.add( new Integer (6) );
		listaPrueba.add( new Integer (4) );
		listaPrueba.add( new Integer (3) );
		listaPrueba.add( new Integer (2) );
		listaPrueba.add( new Integer (1) );
		listaPrueba.add( new Integer (0) );

		listaPrueba.addAtEnd( new Integer (9));

		listaPrueba.addAtK(5, new Integer (5) );

		listaPrueba.next( );


	}

	//Caso 1: la lista está vacía.
	//Caso 2: Sólo hay un elemento en la lista y previous == current.
	//Caso 3: Hay un previous distinto de current
	public void testPrevious() {

		//Caso 1:
		listaPrueba = new RingList<Integer>();
		listaPrueba.previous();
		assertNull(listaPrueba.getCurrentElement());

		//Caso 2: 
		listaPrueba.add(new Integer(0));
		listaPrueba.previous();
		assertNotNull(listaPrueba.getCurrentElement());
		assertTrue(listaPrueba.getCurrentElement().equals(new Integer(0)));

		//Caso 3:
		setUpEscenario1();
		listaPrueba.previous();
		assertTrue(listaPrueba.getCurrentElement().equals(new Integer(1)));

	}

	//Caso 1: la lista está vacía.
	//Caso 2: Sólo hay un elemento en la lista y next == current
	//Caso 3: Hay un next distinto de current.
	public void testNext()
	{
		//Caso 1:
		listaPrueba = new RingList<Integer>();
		listaPrueba.next();
		assertNull(listaPrueba.getCurrentElement());

		//Caso 2: 
		listaPrueba.add(new Integer(0));
		listaPrueba.next();
		assertTrue(listaPrueba.getCurrentElement().equals(new Integer(0)));

		//Caso 3:
		setUpEscenario1();
		listaPrueba.next();
		assertTrue(listaPrueba.getCurrentElement().equals(new Integer(9)));

	}

	//Caso 0: se intenta eliminar un elemento de la lista vacía.
	//Caso 0A: se elimina el único elemento de la lista.
	//Caso 1: se elimina un elemento en la mitad de la lista.
	//Caso 2: se elimina el primer elemento
	//Caso 3: se elemina usando una posición mayor al número de elementos.
	public void testDeleteAtPos() throws Exception {

		//Caso 0:
		listaPrueba = new RingList<Integer>();

		boolean fail=false;
		try {
			listaPrueba.deleteAtPos(0);
		}
		catch(Exception e)
		{
			fail = true;
		}
		assertTrue(fail);

		//Caso 0A
		listaPrueba.add(new Integer (0));
		try {
			listaPrueba.deleteAtPos(0);
		}
		catch(Exception e)
		{
			fail();
		}
		assertEquals((int)listaPrueba.getSize(),0);

		//Caso 1:
		setUpEscenario1();
		try {
			listaPrueba.deleteAtPos(5);
		}
		catch(Exception e)
		{
			fail();
		}
		assertEquals(8, (int)listaPrueba.getSize());
		assertTrue(listaPrueba.getElement(5).equals(new Integer(2)));

		//Caso 2: 
		try {
			listaPrueba.deleteAtPos(0);
		}
		catch(Exception e)
		{
			fail();
		}
		assertEquals(7, (int)listaPrueba.getSize());
		assertTrue(listaPrueba.getElement(0).equals(new Integer(8)));

		//Caso 3:
		try {
			listaPrueba.deleteAtPos(10);
		}
		catch(Exception e)
		{
			fail();
		}
		assertEquals(6, (int)listaPrueba.getSize());
		assertTrue(listaPrueba.getElement(3).equals(new Integer(2)));
	}

	//Caso 1: intenta hacer delete en una lista vacía.
	//Caso 2: se elimina el único elemento de la lista.
	//Caso 3: se elimina el primer elemento.
	//Caso 4: se elimina cualquier elemento.
	public void testDelete()
	{
		//Caso 1:
		boolean fail = false;
		listaPrueba = new RingList<Integer>();
		try {
			listaPrueba.delete();
		}
		catch(Exception e)
		{
			fail = true;
		}
		assertTrue(fail);

		//Caso 2:
		listaPrueba.add(new Integer(0));
		try {
			listaPrueba.delete();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			fail();
		}
		assertEquals(0,(int)listaPrueba.getSize());
		assertNull(listaPrueba.getCurrentElement());

		//Caso 3:
		setUpEscenario1();

		listaPrueba.next();
		try {
			listaPrueba.delete();
		}
		catch(Exception e)
		{
			fail();
		}
		assertTrue(listaPrueba.getCurrentElement().equals(new Integer(8)));
		assertTrue(listaPrueba.getCurrentElement().equals(listaPrueba.getFirst()));

		//Caso 4:
		listaPrueba.next();

		try
		{
			listaPrueba.delete();
		}
		catch (Exception e) {
			fail();
		}
		assertTrue(listaPrueba.getCurrentElement().equals(new Integer(6)));
	}

	//Caso 0: Current element == null
	//Caso 1: El current element != null
	public void testGetCurrentElement()
	{
		//Caso 0:
		listaPrueba = new RingList<Integer>();
		assertNull(listaPrueba.getCurrentElement());

		//Caso 1:
		setUpEscenario1();
		assertEquals(listaPrueba.getCurrentElement(), new Integer(0));
	}

	//Caso 1: Lista vacía
	//Caso 2: elemento en la primera posición
	//Caso 3: elemento al final
	//Caso 4: parámetro mayor al tamaño de la lista.
	public void testGetElement()
	{
		//Caso 1:
		listaPrueba = new RingList<Integer>();
		Integer a = null;
		boolean fail = false;

		try{
			a = (Integer) listaPrueba.getElement(0);
		}
		catch(Exception e)
		{
			fail = true;
		}
		assertTrue(fail);

		//Caso 2:
		setUpEscenario1();

		try {
			a = (Integer) listaPrueba.getElement(0);
		}
		catch(Exception e) {
			fail();
		}
		assertEquals(a,new Integer(9));

		//Caso 3:
		try {
			a = (Integer) listaPrueba.getElement(8);
		}
		catch(Exception e) {
			fail();
		}
		assertEquals(a,new Integer(0));

		//Caso 4:
		try {
			a = (Integer) listaPrueba.getElement(12);
		}
		catch(Exception e) {
			fail();
		}
		assertEquals(a,new Integer(6));

	}
	//Caso 1: se inserta un objeto en la mitad
	//Caso 2: se inserta un objeto al principio
	//Caso 3: se inserta un objeto al final
	//Caso 4: la posición específicada es mayor al tamaño.
	public void testAddAtK() throws Exception
	{
		setUpEscenario1();

		//Caso 1:
		listaPrueba.addAtK(4, new Integer(5));
		assertEquals(new Integer(5), listaPrueba.getElement(4));
		assertEquals((int) listaPrueba.getSize(), 10);

		//Caso 2:
		listaPrueba.addAtK(0, new Integer(10));
		assertEquals(new Integer(10), listaPrueba.getFirst());
		assertEquals((int) listaPrueba.getSize(), 11);

		//Caso 3:
		listaPrueba.addAtK(10,new Integer(-1));
		assertEquals(new Integer(-1), listaPrueba.getElement(10));
		assertEquals((int) listaPrueba.getSize(), 12);

		//Caso 4:
		listaPrueba.addAtK(15,new Integer(69));
		assertEquals(new Integer(69),listaPrueba.getElement(3));
		assertEquals((int) listaPrueba.getSize(), 13);
	}

	//Caso 0: la lista está vacía
	//Caso 1: la lista tiene un objeto.
	//Caso 2: la lista tiene varios objetos
	public void testAddAtEnd()
	{
		//Caso 0:
		listaPrueba = new RingList<Integer>();
		listaPrueba.addAtEnd(new Integer(0));
		assertEquals(listaPrueba.getFirst(), new Integer(0));
		assertEquals((int)listaPrueba.getSize(),1);

		//Caso 1:
		listaPrueba.addAtEnd(new Integer(1));
		assertEquals(listaPrueba.getFirst(), new Integer(0));
		assertEquals((int)listaPrueba.getSize(),2);

		//Caso 2:
		listaPrueba.addAtEnd(new Integer(2));
		assertEquals(listaPrueba.getFirst(), new Integer(0));
		assertEquals((int)listaPrueba.getSize(),3);
	}

	/**
	 * Los métodos add y getSize no serán probados, pues su uso extensivamente en las prubas anteriores 
	 * presupone su funcionamiento correcto.
	 */

	//Caso 1: la lista está vacía.
	//Caso 2: la lista está llena
	public void testIterator()
	{
		//Caso 1:
		listaPrueba = new RingList<Integer>();
		Iterator<Integer> iterador = listaPrueba.iterator();

		int get = -1;

		while(iterador.hasNext()) 
		{
			get = iterador.next();
		}
		assertEquals( get, -1);


		//Caso 2:
		setUpEscenario1();
		iterador = listaPrueba.iterator();

		while(iterador.hasNext()) 
		{
			get = iterador.next();
		}
		assertEquals(get, 0);
	}

}
