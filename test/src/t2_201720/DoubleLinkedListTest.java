package t2_201720;
import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.DoubleLinkedList;
/**
 * �sta clase sirve para verificar
 * @author ma.gomezc
 *
 */
public class DoubleLinkedListTest extends TestCase{

	//------------------------------------
	//Atributos
	//------------------------------------

	/**
	 * �sta es la lista sobre la que se hacen las pruebas
	 */
	private DoubleLinkedList<Integer> listaPrueba;

	//-----------------------------------------
	//M�todos
	//-----------------------------------------
	//

	/**
	 * Construye una lista doblemente encadenada con los n�meros enteros.
	 */
	private void setupEscenario1( )
	{
		listaPrueba = new DoubleLinkedList<Integer>();
		listaPrueba.add( new Integer (0) );
		listaPrueba.add( new Integer (1) );
		listaPrueba.add(new Integer (2) );
		listaPrueba.add(new Integer (3) );
		listaPrueba.add(new Integer (4) );
		listaPrueba.add( new Integer (6) );
		listaPrueba.add(new Integer (7) );
		listaPrueba.add( new Integer (8) );
		listaPrueba.add( new Integer (9) );


	}

	//Caso 1: se inserta un objeto en la mitad
	//Caso 3: se inserta un objeto al final
	//Caso 2: se inserta un objeto al principio
	//Caso 4: la posición específicada es mayor al tamaño.
	public void testAddAtk() throws Exception {

		setupEscenario1();

		//Caso 1.
		Integer a = new Integer(5);
		try{
			listaPrueba.addAtK(3,a);
		}
		catch ( Exception e ){
			fail( "No debería fallar wtf");
		}
		assertEquals("Se esperaba un tamaño de 10", (int) listaPrueba.getSize(),10);
		assertEquals("Se esperaba otro elemento", a, listaPrueba.getElement(3));

		//Caso 2
		a = new Integer(-1);
		try{
			listaPrueba.addAtK(9, a);
		}
		catch(Exception e){
			e.printStackTrace();
			fail( "No debería fallar wtf");
		}
		assertEquals("Se esperaba un tamaño de 11", (int) listaPrueba.getSize(),11);
		assertEquals("Se esperaba otro elemento", a, listaPrueba.getElement(9));

		//Caso 3
		a = new Integer(10);
		try{
			listaPrueba.addAtK(0,a);
		}
		catch(Exception e){
			e.printStackTrace();
			fail( "No debería fallar wtf");
		}
		assertEquals("Se esperaba un tamaño de 12", (int) listaPrueba.getSize(),12);
		assertEquals("Se esperaba otro elemento", a, listaPrueba.getElement(0));

		//Caso 4
		boolean fail = false;
		try {
			//Tamaño 12 => máxima posición 11.
			listaPrueba.addAtK(12, new Integer(12));
		}
		catch(Exception e)
		{
			fail = true;
		}
		if(!fail)
		{
			fail("Debió haber lanzado excepción");
		}
		assertEquals("Se esperaba tamaño 12", (int) listaPrueba.getSize(), 12);
	}

	//Caso 1: Tiene un previous
	//Caso 2: Previous es null
	//Caso 3: current es null
	public void testPrevious() throws Exception
	{		
		setupEscenario1();

		//Caso 1:
		listaPrueba.previous();
		assertEquals("Se esperaba otro valor",listaPrueba.getElement(7), listaPrueba.getCurrentElement());

		//Caso 2:
		for(int i = 0; i<8; i++)
		{
			listaPrueba.previous();
		}
		assertNull(listaPrueba.getCurrentElement());

		//Caso 3:
		try
		{
			listaPrueba.previous();
		}
		catch(Exception e)
		{
			fail("no debería lanzar excepción");

		}



	}

	//Caso 1: next es null
	//Caso 2: tiene un next
	//Caso 3: current es null
	public void testNext() throws Exception
	{
		setupEscenario1();

		//Caso 1:
		listaPrueba.next();
		assertNull( listaPrueba.getCurrentElement());

		//Caso 2:
		listaPrueba.previous();
		listaPrueba.previous();
		listaPrueba.next();
		assertEquals(listaPrueba.getElement(8), listaPrueba.getCurrentElement());

		//Caso 3:
		try
		{
			listaPrueba.next();
			//System.out.println(listaPrueba.getCurrentElement());
		}
		catch(Exception e)
		{
			fail("no debería lanzar excepción");

		}
	}
	
	//Caso 1, se elimina un elemento en la mitad de la lista.
	//Caso 2, se elimina el primer elemento.
	//Caso 3, se elimina el último elemento
	//Caso 4: Se dá por parámetro un índice mayor al tamaño.
	//Caso 5: La lista sólo tiene un elemento.
	public void testDeleteAtPos() throws Exception
	{
		setupEscenario1();

		//Caso 1:
		try {
			listaPrueba.deleteAtPos(4);
		}
		catch(Exception e)
		{
			fail("no debería lanzar excepción");
		}
		assertEquals((int)listaPrueba.getSize(),8);
		assertTrue(listaPrueba.getElement(4).equals(new Integer(3)));

		//Caso 2:
		try {
			listaPrueba.deleteAtPos(0);
		}
		catch(Exception e){
			e.printStackTrace();
			fail("no debería lanzar excepción");
		}
		assertEquals((int)listaPrueba.getSize(),7);
		assertTrue(listaPrueba.getElement(0).equals(new Integer (8)));

		//Caso 3:
		try {
			listaPrueba.deleteAtPos(6);
		}
		catch(Exception e) {
			fail("no debería lanzar excepción");
		}
		assertEquals((int)listaPrueba.getSize(),6);
		assertTrue(listaPrueba.getElement(5).equals(new Integer (1)));

		//Caso 4: 
		boolean fail = false;
		try {
			listaPrueba.deleteAtPos(6);
		}
		catch(Exception e)
		{
			fail = true;
		}
		assertTrue(fail);
		
		listaPrueba = new DoubleLinkedList<Integer>();
		listaPrueba.add(new Integer(0));
		
		
		//Caso 5:
		fail = false;
		try {
		listaPrueba.deleteAtPos(0);
		}
		catch(Exception e) {
			fail("no debería lanzar excepción");
		}
		assertEquals((int)listaPrueba.getSize(),0);
		try {
			assertTrue(listaPrueba.getElement(0).equals(new Integer (0)));
		}
		catch(Exception e)
		{
			fail = true;
		}
		assertTrue(fail);
		
		
	}

	//Caso 1: Hace delete al final de la lista.
	//Caso 2: Hace delete al principio de la lista.
	//Caso 3: Hace delete en la mitad de la lista.
	//Caso 4: Current es null, no se puede eliminar.
	public void testDelete()
	{
		setupEscenario1();

		//Caso 1:
		try
		{
			listaPrueba.delete();
		}
		catch(Exception e)
		{
			fail("No debió haber lanzado excepción");
		}
		assertEquals((int)listaPrueba.getSize(), 8);
		assertNull(listaPrueba.getCurrentElement());

		//Caso 2:
		try {
			listaPrueba.setCurrent(0);

			listaPrueba.delete();
		}
		catch(Exception e)
		{
			fail("No debió haber lanzado excepción");
		}
		assertEquals((int)listaPrueba.getSize(), 7);
		assertTrue(listaPrueba.getCurrentElement().equals(new Integer(8)));

		//Caso 3:
		try {
			listaPrueba.next();
			listaPrueba.next();
			listaPrueba.delete();
		}
		catch(Exception e)
		{
			fail("No debió haber lanzado excepción");
		}
		assertEquals((int)listaPrueba.getSize(), 6);
		assertTrue(listaPrueba.getCurrentElement().equals(new Integer(4)));

		//Caso 4:
		boolean fail = false;
		try {
			listaPrueba.previous();
			listaPrueba.previous();
			listaPrueba.previous();
			listaPrueba.delete();
		}
		catch(Exception e)
		{
			fail = true;
		}
		assertTrue(fail);
	}

	//Caso 1: Current != null.
	//Caso 2: Current == null.
	public void testGetCurrentElement()
	{
		setupEscenario1();

		//Caso 1
		assertTrue(listaPrueba.getCurrentElement().equals(new Integer(0)));

		//Caso 2
		listaPrueba.next();
		assertNull(listaPrueba.getCurrentElement());
	}

	//Caso 1, elemento en la mitad de la lista.
	//Caso 2, elemento al principio.
	//Caso 3, elemento al final
	//Caso 4: Se dá por parámetro un índice mayor al tamaño.
	public void testGetElement()
	{
		setupEscenario1();
		
		Integer a = null;
		
		//Caso 1:
		try {
			a = (Integer)listaPrueba.getElement(4);
		}
		catch(Exception e)
		{
			fail("no debería lanzar excepción");
		}
		assertTrue(a.equals(new Integer(4)));

		//Caso 2:
		try {
			a = (Integer) listaPrueba.getElement(0);
		}
		catch(Exception e){
			fail("no debería lanzar excepción");
		}
		assertTrue(a.equals(new Integer (9)));

		//Caso 3:
		try {
			a = (Integer) listaPrueba.getElement(8);
		}
		catch(Exception e) {
			fail("no debería lanzar excepción");
		}
		assertTrue(a.equals(new Integer (0)));

		//Caso 4: 
		boolean fail = false;
		try {
			a = (Integer) listaPrueba.getElement(15);
		}
		catch(Exception e)
		{
			fail = true;
		}
		assertTrue(fail);
	}
	
	//Caso 0: la lista está vacía
	//Caso 1: la lista tiene un objeto.
	//Caso 2: la lista tiene dos objetos.
	//Caso 3: la lista tiene varios objetos.
	public void addAtEnd() throws Exception
	{
		listaPrueba = new DoubleLinkedList<Integer>();
		
		//Caso 0
		listaPrueba.addAtEnd(new Integer(0));
		assertTrue(listaPrueba.getElement(0).equals(new Integer(0)));
		assertEquals((int)listaPrueba.getSize(), 1);
		
		//Caso 1
		listaPrueba.addAtEnd(new Integer(1));
		assertTrue(listaPrueba.getElement(1).equals(new Integer(1)));
		assertEquals((int)listaPrueba.getSize(), 2);
		
		//Caso 2
		listaPrueba.addAtEnd(new Integer(2));
		assertTrue(listaPrueba.getElement(2).equals(new Integer(2)));
		assertEquals((int)listaPrueba.getSize(), 3);
		
		//Caso 3
		listaPrueba.addAtEnd(new Integer(3));
		assertTrue(listaPrueba.getElement(3).equals(new Integer(3)));
		assertEquals((int)listaPrueba.getSize(), 4);
		
	}
	
	/**
	 * El método add no se probó pues el hecho de que la mayoría de las pruebas usan setupEscenario1()
	 * y este método usa add supone que add está correctamente implementado.
	 */
	/**
	 * Algo similar se puede decir del método getSize();
	 */
	
	//Caso 1: la lista está vacía.
	//Caso 2: la lista está llena
	public void testIterator( )
	{
		//Caso 1:
		listaPrueba = new DoubleLinkedList<Integer>();
		Iterator<Integer> iterador = listaPrueba.iterator();
		
		int get = -1;
		
		while(iterador.hasNext()) 
		{
			get = iterador.next();
		}
		System.out.println(get);
		assertEquals( "El iterador no est� funcionando correctamente", get, -1);
		
		
		//Caso 2:
		setupEscenario1();
		iterador = listaPrueba.iterator();
		
		while(iterador.hasNext()) 
		{
			get = iterador.next();
		}
		System.out.println(get);
		assertEquals( "El iterador no est� funcionando correctamente", get, 0);
	}
}

