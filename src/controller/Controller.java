package controller;

import java.io.File;

import api.ISTSManager;
import model.data_structures.IList;
import model.logic.STSManager;
import model.vo.VORoute;
import model.vo.VOStop;

public class Controller {

	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();

	public static void loadRoutes() {
		manager.loadRoutes("data"+File.separator+"routes.txt");
	}

	public static void loadTrips() {
		manager.loadTrips("data"+File.separator+"trips.txt");
	}

	public static void loadStopTimes() {
		manager.loadStopTimes("data"+File.separator+"stop_times.txt");
	}

	public static void loadStops() {
		manager.loadStops("data"+File.separator+"stops.txt");
	}

	public static IList<VORoute> routeAtStop(String stopName) {
		return manager.routeAtStop(stopName);
	}

	public static IList<VOStop> stopsRoute(String routeName, String direction) {
		return manager.stopsRoute(routeName, direction);
	}
}
