package model.data_structures;

import java.util.Iterator;

//TODO el UML de esta clase.
/**
 * Clase gen�rica de lista doblemente encadenada.
 * @author 
 *
 * @param <T>
 */
public class DoubleLinkedList<T extends Object> implements IList{

	/**
	 * Clase Nodo utilizada para contener los elementos de la lista y hacer la misma gen�rica.
	 */
	private class Node{

		/**
		 * atributo siguiente nodo
		 */
		private Node next;

		/**
		 * atributo previo nodo
		 */
		private Node previous;

		/**
		 * atributo elemento del nodo 
		 */
		private T item;

	}

	/**
	 * Clase iterador para recorrer la lista.
	 * @author Alejo
	 *
	 */
	private class DoubleLinkedListIterator implements java.util.Iterator<T>
	{ 

		private Node current = first;

		public boolean hasNext()
		{
			return current != null;
		}

		public T next()
		{
			T item = (T) current.item;
			current = current.next;
			return item;
		}


	}

	/**
	 * referencia al primer nodo de la lista
	 */
	private Node first;

	/**
	 * referencia al �ltimo nodo de la lista
	 */
	private Node last;

	/**
	 * tama�o de la lista
	 */
	private int size = 0;

	/**
	 * Nodo actual de la lista.
	 */
	//Antes de que se llame el método previous o next, por defecto current = first.
	private Node current; 

	/**
	 * M�todo que inicializa un iterador
	 * @return Iterador de la lista
	 */
	public Iterator iterator() {
		return new DoubleLinkedListIterator();
	}

	/**
	 * M�todo que obtiene el tama�o de la lista
	 * @return tama�o de la lista
	 */
	public Integer getSize() {
		return size;
	}

	//añade al principio.
	public void add (Object item) {

		Node n = new Node();
		n.item = (T) item;

		n.next = first;
		if(first != null) first.previous = n;
		else last = n; //si first == null, entonces la lista estaba vacía por lo que first = last = n. 
		first = n;

		if(current == null) current = first;

		++size;

	}


	/**
	 * M�todo para a�adir un objeto al final de la lista
	 * @param item: objeto por a�adir
	 */
	public void addAtEnd(Object item) {

		Node n = new Node();
		n.item = (T) item;

		n.previous = last;
		if(last != null) last.next = n;
		else first =n; // si last == null, la lista está vacía, por lo tanto last y first deben ser n
		last = n;

		if(current == null) current = first;

		++size;

	}

	/**
	 * M�todo usado para obtener elemento de una determinada posici�n.
	 * @param pos: posici�n de la que se quiere obtener el objeto
	 * @return Elemento de la posici�n indicada
	 */
	public Object getElement(int pos) throws Exception {

		boolean dir;

		if(pos == 0 && size != 0)
			dir = true;
		else if(size/pos >= 2)//Si es igual a 2, el objeto está justamente en la mitad de la lista. Si es mayor está más cerca a first que a last.
			dir = true;

		else if ( ((double) size) / ((double) pos) <= 1.0)//Si size/pos es una fracción pos>size y no está en la lista
			throw new Exception("The position given is larger than the list's size");

		else {
			dir = false;
			pos = (int) (size - pos-1); //Se buscará de derecha a izquierda, por lo tanto pos debe ser la distancia de derecha a izquierda.
		}

		int counter = 0;
		Node n;

		if(dir) {
			n = first;
			while(counter++ < pos){
				n = n.next;
			}
		}
		else {
			n = last;
			while(counter++ < pos){
				n = n.previous;
			}
		}

		return n.item;
	}

	/**
	 * M�todo usado para obtener el objeto de la posici�n actual de iterador.
	 */
	public Object getCurrentElement() {
		if(current != null)
			return current.item;
		return null;
	}

	/**
	 * M�todo para eliminar current y mover el apuntador a la derecha con la excepci�n de que current fuese el �ltimo
	 */
	public void delete() throws Exception {

		if(current!=null) {
			Node next = current.next;
			Node previous = current.previous;

			if(previous != null) previous.next = next;
			if(next != null) next.previous = previous;

			if(current == first)
			{
				first = next;
				current = current.next;

			}
			else if(current == last)
			{
				last = previous;
				current = current.next;
			}
			else
				current = current.next;

			--size;
		}
		else throw new Exception("You are currently positioned over a null element that cannot be deleted");

	}

	/**
	 * Elimina el elemento de la posici�n deseada.
	 * @param pos: posici�n de la que se quiere eliminar el elemento.
	 * @throws Exception
	 */
	public void deleteAtPos(int pos) throws Exception {
		boolean dir;

		if(pos == 0 && size != 0)
			dir = true;
		else if(size/pos >= 2)//Si es igual a 2, el objeto está justamente en la mitad de la lista. Si es mayor está más cerca a first que a last.
			dir = true;

		else if ( ((double) size) / ((double) pos) <= 1.0)//Si size/pos es una fracción pos>size y no está en la lista
			throw new Exception("The position given is larger than the list's size");

		else {
			dir = false;
			pos = (int) (size - pos - 1); //Se buscará de derecha a izquierda, por lo tanto pos debe ser la distancia de derecha a izquierda.
		}

		int counter = 0;
		Node n;

		if(dir) {
			n = first;
			while(counter++ < pos){
				n = n.next;
			}
		}
		else {
			n = last;
			while(counter++ < pos){
				n = n.previous;
			}
		}

		Node previous = n.previous;
		Node next = n.next;

		if(previous != null) previous.next = next;
		if(next != null) next.previous = previous;


		if(n == last)
		{
			if(current == n)
				current = n.previous;
			last = previous;
		}
		else if(n == first)
			first = n.next;

		if(current == n)current = n.next;
		--size;
	}

	/**
	 * Cambia el nodo actual por su siguiente
	 */
	public T next() {
		if(current != null)
			current = current.next;
		else
			current = first;

		if(current!= null)
			return current.item;
		return null;
	}

	/**
	 * Cambia el nodo actual por su anterior
	 */
	public T previous() {
		if(current != null)
			current=current.previous;
		else
			current = last;

		if(current!= null)
			return current.item;
		return null;
	}

	/**
	 * M�todo que a�ade un elemento a una posici�n deseada
	 * @param pos: posici�n en la cual se quiere a�adir el elemento.
	 * @param item: elemento que se quiere a�adir
	 * @throws Exception
	 */
	public void addAtK(int pos, Object item) throws Exception {

		boolean dir;

		if(pos == 0 && size != 0)
			dir = true;
		else if(size/pos >= 2)//Si es igual a 2, el objeto está justamente en la mitad de la lista. Si es mayor está más cerca a first que a last.
			dir = true;

		else if ( pos != 0 &&((double) size) / ((double) pos) <= 1.0)//Si size/pos es una fracción pos>size y no está en la lista
			throw new Exception("The position given is larger than the list's size");
		else {
			dir = false;
			pos = (int) (size - pos - 1); //Se buscará de derecha a izquierda, por lo tanto pos debe ser la distancia de derecha a izquierda.
		}

		int counter = 0;
		Node n;

		if(dir) {
			n = first;
			//<= porque quiero ubicarme en la posición en la que haré el intercambio (< me ubicaría una pos a la izquierda)
			while(counter++ < pos){
				n = n.next;
			}
		}
		else {
			n = last;
			while(counter++ < pos){
				n = n.previous;
			}
		}

		Node n1 = new Node();
		n1.item = (T) item;

		if(n != first) {
			Node previous = n.previous;
			n1.previous = previous;
			n1.next = n;
			n.previous = n1;
			previous.next = n1;
		}
		else {
			n1.next = n;
			n.previous = n1;
			first = n1;
		}
		++size;

		if(current == null) current = first;
	}
	
	/**
	 * M�todo usado paar obtener el nodo de determinada posici�n
	 * @param pos: posici�n de la que se quiere obtener el nodo.
	 * @return Nodo de la posici�n pos.
	 * @throws Exception
	 */
	private Node getNode(int pos) throws Exception
	{
		boolean dir;

		if(pos == 0 && size != 0)
			dir = true;
		else if(size/pos >= 2)//Si es igual a 2, el objeto está justamente en la mitad de la lista. Si es mayor está más cerca a first que a last.
			dir = true;

		else if ( ((double) size) / ((double) pos) <= 1.0)//Si size/pos es una fracción pos>size y no está en la lista
			throw new Exception("The position given is larger than the list's size");

		else {
			dir = false;
			pos = (int) (size - pos-1); //Se buscará de derecha a izquierda, por lo tanto pos debe ser la distancia de derecha a izquierda.
		}

		int counter = 0;
		Node n;

		if(dir) {
			n = first;
			while(counter++ < pos){
				n = n.next;
			}
		}
		else {
			n = last;
			while(counter++ < pos){
				n = n.previous;
			}
		}

		return n;
	}
	
	/**
	 * M�todo para cambiar el nodo actual por el de la posici�n deseada.
	 * @param pos: posici�n de la que se quiere obtener el nodo
	 * @throws Exception
	 */
	public void setCurrent(int pos) throws Exception
	{
		current = getNode(pos);
	}

}
