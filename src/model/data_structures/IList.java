package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> {

	Integer getSize();
	
	void add(T item);
	
	void addAtEnd(T item);;
	
	void addAtK(int pos, T item) throws Exception;
	
	T getElement(int pos) throws Exception;
	
	T getCurrentElement();
	
	void delete() throws Exception;
	
	void deleteAtPos(int pos)throws Exception;
	
	T next();
	
	T previous();
 
	void setCurrent(int pos) throws Exception;

	
}