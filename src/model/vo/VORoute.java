package model.vo;

/**
 * Representation of a route object
 */
public class VORoute {

	private int id;
	
	private String agencyId;
	
	private String routeShortName;
	
	private String routeLongName;
	
	private String desc;
	
	private byte routeType;
	
	private String routeUrl;
	
	private String routeColor;
	
	private String routeTextColor;
	
	public VORoute(int id, String agencyId, String routeShortName, String routeLongName, String desc, byte routeType, String routeUrl, String routeColor, String routeTextColor)
	{
		this.id = id;
		this.agencyId = agencyId;
		this.routeShortName = routeShortName;
		this.routeLongName = routeLongName;
		this.desc = desc;
		this.routeType = routeType;
		this.routeUrl = routeUrl;
		this.routeColor = routeColor;
		this.routeTextColor = routeTextColor;
	}
	
	public int id() {
		return id;
	}
	
	public String agencyId() {
		return agencyId;
	}
	
	public String routeShortName() {
		return routeShortName;
	}

	/**
	 * @return name - route name
	 */
	public String getName() {
		return routeLongName;
	}
	
	public String desc() {
		return desc;
	}
	
	public byte routeType() {
		return routeType;
	}
	
	public String routeUrl() {
		return routeUrl;
	}
	
	public String routeColor() {
		return routeColor;
	}
	
	public String routeTextColor() {
		return routeTextColor;
	}

}
