package model.vo;

public class VOStopTimes {
	
	public int tripId;
	
	public String arrivalTime;
	
	public String departureTime;
	
	public int stopId;
	
	public byte stopSequence;
	
	public String stopHeadSign;
	
	public byte pickUpType;
	
	public byte dropOffType;
	//Puede ser igual a null (para la primera parada de cada ruta).
	public Double shapeDistanceTravelled;

	public VOStopTimes(int tripId, String arrivalTime, String departureTime, int stopId, byte stopSequence, String stopHeadSign, byte pickUpType, byte dropOffType, Double shapeDistanceTravel)
	{
		this.tripId = tripId;
		this.arrivalTime = arrivalTime;
		this.departureTime = departureTime;
		this.stopId = stopId;
		this.stopSequence = stopSequence;
		this.stopHeadSign = stopHeadSign;
		this.pickUpType = pickUpType;
		this.dropOffType = dropOffType;
	}
	
	public int tripId() {
		return tripId;
	}
	
	public String arrivalTime() {
		return arrivalTime;
	}
	
	public String departureTime() {
		return departureTime;
	}
	
	public int stopId() {
		return stopId;
	}
	
	public byte stopSequence() {
		return stopSequence;
	}
	
	public String stopHeadSign() {
		return stopHeadSign;
	}
	
	public byte pickUpType() {
		return pickUpType;
	}
	
	public byte dropOffType() {
		return dropOffType;
	}
	
	public Double shapeDistanceTravelled() {
		return shapeDistanceTravelled;
	}
}
