package model.vo;

/**
 * Representation of a Stop object
 */
public class VOStop{
	
	private int id;
	
	private Integer stopCode;
	
	private String stopName;
	
	private String stopDesc;
	
	private double stopLat;
	
	private double stopLon;
	
	private String zoneId;
	
	private String stopUrl;
	
	private byte locationType;
	
	private String parentStation;
	
	public VOStop(int id, Integer stopCode, String stopName, String stopDesc, double stopLat, double stopLon, String zoneId, String stopUrl, byte locationType, String parentStation)
	{
		this.id = id;
		this.stopCode = stopCode;
		this.stopName = stopName;
		this.stopDesc = stopDesc;
		this.stopLat = stopLat;
		this.stopLon = stopLon;
		this.zoneId = zoneId;
		this.stopUrl = stopUrl;
		this.locationType = locationType;
	}
	
	public int id() {
		return id;
	}
	
	public int stopCode() {
		return stopCode;
	}
	
	public String stopDesc() {
		return stopDesc;
	}
	
	public double stopLat() {
		return stopLat;
	}
	
	public double stopLon() {
		return stopLon;
	}
	
	public String zoneId() {
		return zoneId;
	}
	
	public String stopUrl() {
		return stopUrl;
	}
	
	public byte locationType() {
		return locationType;
	}
	
	public String parentStation() {
		return parentStation;
	}
	

	/**
	 * @return name - stop name
	 */
	public String getName() {
		return stopName;
	}

}
