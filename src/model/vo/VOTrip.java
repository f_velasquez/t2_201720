package model.vo;

public class VOTrip {

	private int routeId;
	
	private int serviceId;
	
	private int id;
	
	private String tripHeadSign;
	
	private String tripShortName;
	
	private byte directionId;
	
	private int blockId;
	
	private int shapeId;
	
	private boolean wheelChairAccessible;
	
	private boolean bikesAllowed;
	
	public VOTrip(int routeId, int serviceId, int id, String tripHeadSign, String tripShortName, byte directionId, int blockId, int shapeId, byte wheelChairAccessible, byte bikesAllowed)
	{
		this.routeId = routeId;
		this.serviceId = serviceId;
		this.id = id;
		this.tripHeadSign = tripHeadSign;
		this.tripShortName  = tripShortName;
		this.directionId = directionId;
		this.blockId = blockId;
		this.shapeId = shapeId;
		this.wheelChairAccessible = (wheelChairAccessible == 1);
		this.bikesAllowed = (bikesAllowed == 1);
	}
	
	public int routeId()
	{
		return routeId;
	}
	
	public int serviceId()
	{
		return serviceId;
	}
	
	public int id()
	{
		return id;
	}
	
	public String tripHeadSign()
	{
		return tripHeadSign;
	}
	
	public String tripShortname()
	{
		return tripShortName;
	}
	
	public byte directionId()
	{
		return directionId;
	}
	
	public int shapeId() {
		return shapeId;
	}
	
	public boolean wheelChairAccessible() {
		return wheelChairAccessible;
	}
	
	public boolean bikesAllowed() {
		return bikesAllowed;
	}
}
