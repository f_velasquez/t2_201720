package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Iterator;

import api.ISTSManager;
import model.vo.*;
import model.data_structures.*;
/**
 * Clase encargada de manejar las listas
 * @author Alejo
 *
 */
public class STSManager implements ISTSManager {

	/**
	 * Lista doblemente encadenada de las rutas.
	 */
	private DoubleLinkedList<VORoute> routes;

	/**
	 * Lista doblemente encadenada de los tiempos de paradas
	 */
	private DoubleLinkedList<VOStopTimes> stopTimes;

	/**
	 * Lista circular de los viajes
	 */
	private RingList<VOTrip> trips;

	/**
	 * Lista circular de las paradas
	 */
	private RingList<VOStop> stops;

	/**
	 * M�todo encargado de cargar las rutas en la lista.
	 * @param routesFile: ruta del archivo desde el cual se cargan las listas
	 */
	public void loadRoutes(String routesFile){

		routes = new DoubleLinkedList<VORoute>();

		try {
			BufferedReader bf = new BufferedReader(new FileReader( new File(routesFile)));

			String ln1 = bf.readLine();
			//La primera línea se ignora
			ln1 = bf.readLine();

			VORoute route;

			while(ln1 != null) {
				String[] info =  ln1.split(",");
				route = new VORoute(Integer.parseInt(info[0]), info[1].trim(), info[2].trim(), info[3].trim(), info[4].trim(), Byte.parseByte(info[5]), info[6].trim(), info[7].trim(), info[8].trim());
				routes.addAtEnd(route);
				ln1 = bf.readLine();
			}
			bf.close();
		}
		catch(Exception e)
		{
			System.out.println("Unexpected error loading the routes file");
		}

	}

	/**
	 * M�todo encargado de cargar los viajes en la lista
	 * @param tripsFile: ruta del archivo desde el cual se cargan los viajes.
	 */
	@Override
	public void loadTrips(String tripsFile) {
		trips = new RingList<VOTrip>( );

		//Carga la lista ordenando los trips por routeId
		try
		{
			BufferedReader bf = new BufferedReader( new FileReader( new File( tripsFile ) ) );
			String ln = bf.readLine( );
			ln = bf.readLine( );

			VOTrip trip;
			String[] info ;
			Integer idd;
			int i;
			VOTrip comp;
			int size;
			boolean termino;
			System.out.println("Iniciando la lectura del archivo, esto puede tardar un momento:");
			while ( ln != null )
			{
				info = ln.split(",");
				idd = (Integer.parseInt(info[0]));
				trip = new VOTrip(Integer.parseInt(info[0]), Integer.parseInt(info[1]), Integer.parseInt(info[2]), info[3].trim(), info[4].trim(), Byte.parseByte(info[5]), Integer.parseInt(info[6]), Integer.parseInt(info[7]), Byte.parseByte(info[8]), Byte.parseByte(info[9]));

				if(trips.getSize( ) == 0)
				{
					trips.addAtEnd(trip);
				}
				else
				{
					//Organiza los primero por id de ruta y después por id trip.
					termino = false;
					i = 0;
					trips.setCurrent(0);
					size = trips.getSize();
					while (i < size && !termino)
					{
						comp = (VOTrip) trips.getCurrentElement();
						if(!(comp.routeId() < idd) && !(comp.routeId() == idd)) {
							trips.addAtK(i, trip);
							termino = true;
						}
						else if(comp.routeId() == idd)
						{
							if(comp.id() > trip.id())
							{
								trips.addAtK(i, trip);
								termino = true;
							}
						}


						i++;
						trips.next();
					}
					if(!termino)
					{
						trips.addAtEnd(trip);
					}
				}
				ln = bf.readLine( );
			}
			bf.close();
			
			/*int a = 0;
			VOTrip t;
			while(a<trips.getSize())
			{
				t = (VOTrip) trips.getElement(a);
				System.out.println(t.routeId() + " : " +t.id());
				a++;
			}*/

			System.out.println("Terminó de leer archivo de texto");

			
		}
		catch( Exception e )
		{
			e.printStackTrace();
			System.out.println("Unexpected error loading the trips file");
		}

		System.out.println("Tamaño de la lista: "+ trips.getSize());
	}

	/**
	 * M�todo encargado de cargar los tiempos de parada en la lista.
	 * @param stopTimesFile: ruta del archivo desde el cual se cargar�n los tiempos de parada.
	 */
	public void loadStopTimes(String stopTimesFile) {

		
		System.out.println("Cargando stopTimes, esto puede tardar un rato");
		stopTimes = new DoubleLinkedList<VOStopTimes>();
		try {
			BufferedReader bf = new BufferedReader(new FileReader( new File(stopTimesFile)));

			String ln1 = bf.readLine();
			//La primera línea se ignora
			ln1 = bf.readLine();

			VOStopTimes stopTimes;
			String[] info;
			Double inf8;
			while(ln1 != null) {
				info =  ln1.split(",");
				try {
					//Intenta atrapar un arrayIndex out of bounds
					inf8 = new Double(info[8]);
				}
				catch(ArrayIndexOutOfBoundsException ae)
				{
					inf8=null;
				}
				stopTimes = new VOStopTimes(Integer.parseInt(info[0]), info[1].trim(), info[2].trim(), Integer.parseInt(info[3]), Byte.parseByte(info[4]), info[5].trim(), Byte.parseByte(info[6]), Byte.parseByte(info[7]), inf8 );
				this.stopTimes.addAtEnd(stopTimes);
				ln1 = bf.readLine();
			}
			bf.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println("Unexpected error loading the stopTimes file");
		}
		System.out.println("Tamaño de la lista: " + stopTimes.getSize());
	}

	/**
	 * M�todo encargado de cargar las paradas en la lista.
	 * @param stopsFile: ruta del archivo desde el cual se cargar�n las paradas.
	 */
	@Override
	public void loadStops(String stopsFile) {
		stops = new RingList<VOStop>( );

		System.out.println("Leyendo archivo, esto puede tomar un momento");

		try
		{
			BufferedReader bf = new BufferedReader( new FileReader( new File( stopsFile ) ) );
			String ln = bf.readLine( );
			ln = bf.readLine( );

			VOStop stop;

			boolean termino = false;
			int i;
			VOStop comp;
			int size;
			String inf9;
			String[] info;
			String zoneId;
			Integer inf1;
			while ( ln != null )
			{ 
				info= ln.split(",");
				zoneId = info[6];

				try {
					inf9 = info[9].trim();
				}
				catch(ArrayIndexOutOfBoundsException ae) {
					inf9 = null;
				}
				try {
					inf1 = Integer.parseInt(info[1]);
				}
				catch(NumberFormatException ne)
				{
					inf1 = null;
				}

				stop = new VOStop(Integer.parseInt(info[0]), inf1, info[2].trim(), info[3].trim(), Double.parseDouble(info[4]), Double.parseDouble(info[5]), info[6].trim(), info[7].trim(), Byte.parseByte(info[8]),inf9);

				if(stops.getSize( ) == 0)
				{
					stops.addAtEnd(stop);
				}
				else
				{
					//Organiza las zonas alfanuméricamente y después por nombres de los stops
					termino = false;
					i = 0;
					stops.setCurrent(0);
					size = stops.getSize();
					while (i < size && !termino)
					{
						comp = (VOStop) stops.getCurrentElement();
						if(!( comp.zoneId().compareTo(zoneId)<= 0) && !(comp.zoneId().equals(zoneId))){
							stops.addAtK(i, stop);
							termino = true;
						}
						else if(comp.zoneId().equals(zoneId))
						{
							if(comp.getName().compareTo(stop.getName())>=0)
							{
								stops.addAtK(i, stop);
								termino = true;
							}
						}

						i++;
						stops.next();
					}
					if(!termino)
					{
						stops.addAtEnd(stop);
					}
				}
				ln = bf.readLine( );
			}
			bf.close();

			/*int a = 0;
			VOStop t;
			while(a<stops.getSize())
			{
				t = (VOStop) stops.getElement(a);
				System.out.println(t.zoneId() + " : " +t.getName());
				a++;
			}*/

			
			System.out.println("Terminó de leer archivo de texto");
		}
		
		
		
		catch( Exception e )
		{
			e.printStackTrace();
			System.out.println("Unexpected error loading the stops file");
		}

		System.out.println("tamaño de la lista: " +stops.getSize());

	}

	/**
	 * M�todo encargado de retornar las rutas que se detienen en una parada espec�fica
	 * @param pStopName: Nombre de la parada en la cu�l paran las rutas que se quieren obtener.
	 */
	@Override
	public IList<VORoute> routeAtStop(String pStopName) {
		
		Iterator it = stops.iterator();
		int stopId = -1;
		VOStop tmp = null;
				
		while(it.hasNext())
		{
			tmp = (VOStop) it.next();
			if(tmp.getName().equals(pStopName))
			{
				stopId = tmp.id();
				break;
			}
		}
		if(stopId == -1)
		{
			System.out.println("No se encontró stop con el nombre dado");
		}
		
		
		it = stopTimes.iterator();
		DoubleLinkedList<Integer> tripIds = new DoubleLinkedList<Integer>();
		VOStopTimes tmpst = null;
		
		while(it.hasNext())
		{
			tmpst = (VOStopTimes) it.next();
			if(tmpst.stopId == stopId)
			{
				tripIds.add(tmpst.tripId);
			}
		}
		if(tripIds.getSize() == 0)
		{
			System.out.println("No se encontraron tripIds con el stopId");
		}
		
		it = trips.iterator();
		Iterator it2;
		DoubleLinkedList<Integer> routeIds = new DoubleLinkedList<Integer>();
		VOTrip tmpt = null;
		
		while(it.hasNext())
		{
			tmpt = (VOTrip) it.next();
			it2 = tripIds.iterator();
			while(it2.hasNext())
			{
				if(tmpt.id() == (int)it2.next())
				{
					routeIds.add(tmpt.routeId());
				}
			}
		}
		if(routeIds.getSize() == 0)
		{
			System.out.println("No se encontraron rutas con el trip id dado");
		}
		
		it = routes.iterator();
		DoubleLinkedList<VORoute> rtas = new DoubleLinkedList<VORoute>();
		VORoute tmpr = null;
		
		while(it.hasNext())
		{
			tmpr = (VORoute) it.next();
			it2 = routeIds.iterator();
			while(it2.hasNext())
			{
				if(tmpr.id()== (int) it2.next())
				{
					rtas.add(tmpr);
				}
			}
		}
		
		try {
			rtas.setCurrent(0);
			tmpr = (VORoute) rtas.getCurrentElement();
			String n = tmpr.getName();
			
			tmpr = rtas.next();
			while(tmpr!=null)
			{
				
				if(tmpr.getName().equals(n))
				{
					rtas.delete();
					tmpr = (VORoute) rtas.getCurrentElement();
				}
				else {
					n = tmpr.getName();
					tmpr = rtas.next();
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return rtas;
	}

	/**
	 * M�todo encargado de retornar las paradas que se encuentran en una ruta en la direcci�n espec�ficada, dada una direcci�n de viaje
	 * @param routeName: Nombre de la ruta que se quiere buscar
	 * @param direction: Nombre de la direcci�n.
	 */
	@Override
	public IList<VOStop> stopsRoute(String routeName, String direction) {

		Iterator it = routes.iterator();
		int routeId = -1;
		VORoute tmp = null;
		while(it.hasNext()){
			tmp = (VORoute) it.next();
			if(tmp.getName().equals(routeName.trim()))
			{
				routeId = tmp.id();
				break;
			}
		}
		if(routeId == -1)
		{
			System.out.println("No se encontró ruta");
		}


		it = trips.iterator();
		VOTrip tmpp = null;
		DoubleLinkedList<Integer>tripIds = new DoubleLinkedList<Integer>();

		while(it.hasNext()) {
			tmpp = (VOTrip) it.next();
			if(tmpp.routeId() == routeId && (tmpp.directionId() == Byte.parseByte(direction)))
			{
				tripIds.add(tmpp.id());
			}
		}
		if(tripIds.getSize()==0) {
			System.out.println("No se encontraron viajes en la ruta y dirección dada");
		}
		System.out.println("tripidssize: " +tripIds.getSize());


		it = stopTimes.iterator();
		Iterator it2;

		VOStopTimes tmpst = null;
		DoubleLinkedList<Integer> stopIds = new DoubleLinkedList<Integer>();

		while(it.hasNext()){
			tmpst = (VOStopTimes) it.next();
			it2 = tripIds.iterator();

			while(it2.hasNext())
			{
				if(tmpst.tripId() == (int)it2.next())
				{
					stopIds.add(tmpst.stopId);
				}
			}
		}
		if(stopIds.getSize()==0) {
			System.out.println("No se encontraron paradas en el viaje dado");
		}

		System.out.println("stopidsize: " + stopIds.getSize());
		it = stops.iterator();

		VOStop tmps = null;
		DoubleLinkedList<VOStop> stopNames = new DoubleLinkedList<VOStop>();

		while(it.hasNext())
		{
			tmps = (VOStop) it.next();
			it2 = stopIds.iterator();

			while(it2.hasNext())
			{
				if(tmps.id() == (int) it2.next())
				{
					stopNames.add(tmps);
				}

			}
		}

		//Elimina duplicados;
		try {
			stopNames.setCurrent(0);
			tmps = (VOStop) stopNames.getCurrentElement();
			String n = tmps.getName();
			
			tmps = stopNames.next();
			while(tmps!=null)
			{
				
				if(tmps.getName().equals(n))
				{
					stopNames.delete();
					tmps = (VOStop) stopNames.getCurrentElement();
				}
				else {
					n = tmps.getName();
					tmps = stopNames.next();
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return stopNames;
	}
}
